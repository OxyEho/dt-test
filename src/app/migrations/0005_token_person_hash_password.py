# Generated by Django 4.0.4 on 2022-05-25 21:49

import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0004_transaction"),
    ]

    operations = [
        migrations.CreateModel(
            name="Token",
            fields=[
                ("access_token", models.UUIDField(editable=False, unique=True)),
                ("refresh_token", models.UUIDField(editable=False, unique=True)),
                (
                    "person",
                    models.OneToOneField(
                        editable=False,
                        on_delete=django.db.models.deletion.CASCADE,
                        primary_key=True,
                        serialize=False,
                        to="app.person",
                    ),
                ),
                ("creation_time", models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ("is_useful", models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name="person",
            name="hash_password",
            field=models.CharField(editable=False, max_length=255, null=True),
        ),
    ]
