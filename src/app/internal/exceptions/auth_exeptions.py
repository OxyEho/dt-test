class RefreshExpired(Exception):
    pass


class NoSetPassword(Exception):
    pass


class IncorrectPassword(Exception):
    pass


class IncorrectToken(Exception):
    pass
