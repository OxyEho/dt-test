class CardNotFound(Exception):
    pass


class NotEnoughMoney(Exception):
    def __init__(self, balance):
        self.balance = balance


class NotInFavourite(Exception):
    def __init__(self, username):
        self.username = username


class IncorrectRecipient(Exception):
    def __init__(self, recipient):
        self.recipient = recipient


class DifferentCurrency(Exception):
    pass
