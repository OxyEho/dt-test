class FavouriteNotFound(Exception):
    def __init__(self, owner_username: str, favourite_username: str):
        self.owner_username = owner_username
        self.favourite_username = favourite_username
