import json
import uuid

from django.http import HttpResponseBadRequest, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt

import app.internal.services.auth_service as auth_service
from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.token_dto import TokenDTO
from app.internal.exceptions.auth_exeptions import IncorrectPassword, IncorrectToken, RefreshExpired


def me(request, pk):
    if request.method != "GET":
        return HttpResponseNotAllowed(["GET"])
    if not request.person or request.person.person_id != pk:
        return JsonResponse({"msg": "Unauthorized"}, status=401)
    if not request.person.phone:
        return JsonResponse({"msg": "You need to specify phone"})
    return JsonResponse(request.person.to_dict())


@csrf_exempt
def login(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    payload = json.loads(request.body)
    try:
        person_dto = PersonDTO(username=payload["username"], password=payload["password"])
        token = auth_service.login(person_dto)
        return JsonResponse(token.to_dict())
    except IncorrectPassword:
        return JsonResponse({"msg": "Incorrect password"})
    except KeyError:
        return HttpResponseBadRequest()


@csrf_exempt
def refresh(request):
    if request.method != "POST":
        return HttpResponseNotAllowed(["POST"])
    payload = json.loads(request.body)
    try:
        token_dto = TokenDTO(refresh_token=uuid.UUID(payload["refresh_token"]))
        token_dto = auth_service.refresh_access(token_dto)
        return JsonResponse(token_dto.to_dict())
    except RefreshExpired:
        return JsonResponse({"msg": "Tokens expired"})
    except IncorrectToken:
        return JsonResponse({"msg": "Incorrect token"})
    except KeyError:
        return HttpResponseBadRequest()
