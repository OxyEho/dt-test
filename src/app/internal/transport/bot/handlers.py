from functools import wraps
from typing import List

from telegram import Update
from telegram.ext import CallbackContext

import app.internal.services.auth_service as auth_service
from app.internal.bot_stages.transfer_stage import TransferStage, TransferStageEnum
from app.internal.dto.account_dto import AccountDTO
from app.internal.dto.card_dto import CardDTO
from app.internal.dto.favourite_dto import FavouriteDTO
from app.internal.dto.person_dto import PersonDTO, ValidationException
from app.internal.dto.person_list_dto import PersonListDTO
from app.internal.exceptions.favourite_exceptions import FavouriteNotFound
from app.internal.exceptions.person_exceptions import PersonNotFound
from app.internal.exceptions.transfer_exceptions import (
    CardNotFound,
    DifferentCurrency,
    IncorrectRecipient,
    NotEnoughMoney,
    NotInFavourite,
)
from app.internal.models.card import Card
from app.internal.models.person import Person
from app.internal.services.bank_service import BankService
from app.internal.services.person_service import PersonService


class BotMsg:
    HELP = (
        "/start - getting started, the bot saves information about you"
        + "\n/set_phone [phone] - bot saves your phone number"
        + "\n/me - bot shows info about you"
        + "\n/card [card_id] - shows card balance"
        + "\n/account [account_id] - shows account balance"
        + "\n/add_favourite [username] - add username to favourites"
        + "\n/show_favourite"
        + "\n/delete_favourite [username] - delete username from favourites"
        + "\n/send_money - send money to favourite user"
        + "\n/extract - info about bank account"
        + "\n/activity - persons who transferred money to you and persons to whom you transferred money"
    )
    SAVE_PHONE = "Phone saved"
    INCORRECT_PHONE = "Incorrect phone"
    NO_PHONE = "Phone not entered"
    NO_ME_PHONE = "You can not get the information until you specify phone"
    CARD_NOT_FOUND = "Card not found"
    NO_CARD_ID = "Please, enter the card id"
    ACCOUNT_NOT_FOUND = "Account not found"
    NO_ACCOUNT_ID = "Please, enter the account id"
    USER_NOT_FOUND = "User not found"
    FAVOURITE_ADDED = "Favourite added"
    FAVOURITE_DELETED = "Favourite deleted"
    SENDER_CARD = "Enter sender card"
    ENTER_VALUE = "Enter value"
    RECIPIENT_CARD = "Enter recipient card"
    ENTER_RECIPIENT = "Enter username or account id"
    INCORRECT_CURRENCY = "Incorrect currency"
    SUCCESS = "Success"
    TRANSFER_CANCEL = "Transfer canceled"
    SWW = "Something went wrong, check input"
    FAVOURITE_BEGIN = "Favourites of"
    ACTIVITY_BEGIN = "Activity persons of"
    SET_PASSWD = "Password saved"
    INCORRECT_PASSWD = "Incorrect password"

    @staticmethod
    def get_person_info_msg(person: Person) -> str:
        person_info = person.to_dict()
        text_list = []
        for field, value in person_info.items():
            text_list.append(f'{field.capitalize()}: {value if value else "Not found"}')
        return "\n".join(text_list)

    @staticmethod
    def get_hello(name: str) -> str:
        return f"Hello, {name}"

    @staticmethod
    def get_account_cards_msg(cards: List[Card]) -> str:
        return "\n".join([card.msg_str() for card in cards])

    @staticmethod
    def favourite_not_found(owner_username: str, favourite_username: str):
        return f"Favourite {owner_username} : {favourite_username} not found"

    @staticmethod
    def person_not_found(username: str):
        return f"Person {username} not found"

    @staticmethod
    def favourite_msg(favourites_dto: PersonListDTO):
        return f"Favourites of {favourites_dto.username}:\n" + "\n".join(favourites_dto.chosen_usernames)

    @staticmethod
    def not_enough_money(balance):
        return f"Not Enough money: {balance}"

    @staticmethod
    def not_in_favourite(username):
        return f"Not in favourite: {username}"

    @staticmethod
    def incorrect_recipient(recipient):
        return f"Incorrect recipient: {recipient}"


TRANSFER_STAGES: dict[PersonDTO, TransferStage] = {}


def exception_decorator(func):
    @wraps(func)
    def exception_wrapper(update: Update, context: CallbackContext):
        try:
            func(update, context)
        except Exception:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.SWW)
            raise

    return exception_wrapper


@exception_decorator
def start(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update)
    PersonService.save(person_dto)
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.get_hello(update.effective_user.username))


@exception_decorator
def set_phone(update: Update, context: CallbackContext):
    try:
        person_dto = PersonDTO.get_instance(update)
        person_dto.set_phone(context)
        PersonService.update(person_dto)
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.SAVE_PHONE)
    except ValidationException:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.INCORRECT_PHONE)


@exception_decorator
def me(update: Update, context: CallbackContext):
    person = Person.objects.filter(person_id=update.effective_user.id).first()
    if person.phone:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.get_person_info_msg(person))
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.NO_ME_PHONE)


@exception_decorator
def help_(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.HELP)


@exception_decorator
def card_balance(update: Update, context: CallbackContext):
    try:
        card_dto = CardDTO.get_instance(update=update, context=context)
        card = BankService.get_card(card_dto)
        context.bot.send_message(chat_id=update.effective_chat.id, text=card.msg_str())
    except Card.DoesNotExist:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.CARD_NOT_FOUND)


@exception_decorator
def account_balance(update: Update, context: CallbackContext):
    account_dto = AccountDTO.get_instance(update=update, context=context)
    cards = BankService.get_account_cards(account_dto)
    if cards:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.get_account_cards_msg(cards))
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.ACCOUNT_NOT_FOUND)


@exception_decorator
def add_favourite(update: Update, context: CallbackContext):
    try:
        favourite_dto = FavouriteDTO.get_instance(update=update, context=context)
        PersonService.add_favourite(favourite_dto)
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.FAVOURITE_ADDED)
    except PersonNotFound as e:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.person_not_found(e.username))


@exception_decorator
def delete_favourite(update: Update, context: CallbackContext):
    try:
        favourite_dto = FavouriteDTO.get_instance(update=update, context=context)
        PersonService.delete_favourite(favourite_dto)
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.FAVOURITE_DELETED)
    except FavouriteNotFound as e:
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=BotMsg.favourite_not_found(e.owner_username, e.favourite_username)
        )
    except PersonNotFound as e:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.person_not_found(e.username))


@exception_decorator
def show_favourites(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    person_list_dto = PersonService.get_favourites(person_dto)
    context.bot.send_message(chat_id=update.effective_chat.id, text=person_list_dto.to_msg(BotMsg.FAVOURITE_BEGIN))


@exception_decorator
def send_money(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    TRANSFER_STAGES[person_dto] = TransferStage(person_dto)
    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.SENDER_CARD)


@exception_decorator
def cancel_transfer(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    transfer_stage = TRANSFER_STAGES.get(person_dto)
    if transfer_stage:
        del TRANSFER_STAGES[person_dto]
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.TRANSFER_CANCEL)


@exception_decorator
def set_password(update: Update, context: CallbackContext):
    try:
        person_dto = PersonDTO.get_instance(update=update)
        person_dto.set_password(context)
        auth_service.set_password(person_dto)
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.SET_PASSWD)
    except ValidationException:
        context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.INCORRECT_PASSWD)


@exception_decorator
def transfer(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    transfer_stage = TRANSFER_STAGES.get(person_dto)
    if transfer_stage:
        try:
            BankService.update_transfer(transfer_stage, update.message.text)
            match transfer_stage.stage:
                case TransferStageEnum.SENDER_CARD_ENTERED:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.ENTER_VALUE)
                case TransferStageEnum.VALUE_ENTERED:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.ENTER_RECIPIENT)
                case TransferStageEnum.RECIPIENT_ENTERED:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.RECIPIENT_CARD)
                case TransferStageEnum.RECIPIENT_CARD_ENTERED:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.SUCCESS)
                    del TRANSFER_STAGES[person_dto]
            if TransferStageEnum.RECIPIENT_CARD_ENTERED != transfer_stage.stage:
                transfer_stage.next_stage()

        except CardNotFound:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.CARD_NOT_FOUND)
        except NotEnoughMoney as e:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.not_enough_money(e.balance))
        except NotInFavourite as e:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.not_in_favourite(e.username))
        except IncorrectRecipient as e:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.incorrect_recipient(e.recipient))
        except DifferentCurrency:
            context.bot.send_message(chat_id=update.effective_chat.id, text=BotMsg.INCORRECT_CURRENCY)


@exception_decorator
def extract(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    transactions_dto = BankService.get_account_extract(person_dto)
    context.bot.send_message(chat_id=update.effective_chat.id, text=transactions_dto.to_msg())


@exception_decorator
def activity(update: Update, context: CallbackContext):
    person_dto = PersonDTO.get_instance(update=update)
    person_list_dto = BankService.get_activity_persons(person_dto)
    context.bot.send_message(chat_id=update.effective_chat.id, text=person_list_dto.to_msg(BotMsg.ACTIVITY_BEGIN))
