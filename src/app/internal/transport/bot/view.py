import json

from django.http import JsonResponse
from django.views import View

from app.internal.bot import WebhookBot


class WebhookBotView(View):
    def post(self, request):
        bot = WebhookBot()
        bot.process_update(json.loads(request.body))
        return JsonResponse({"ok": "update processed"})

    def get(self, request):
        return JsonResponse({"ok": "update not processed"})
