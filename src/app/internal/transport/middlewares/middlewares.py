import uuid

from app.internal.dto.token_dto import TokenDTO
from app.internal.services import auth_service


def auth_middleware(get_response):
    def middleware(request):
        request.person = None
        access_token_str = request.COOKIES.get("access_token")
        username = request.COOKIES.get("username")
        if access_token_str and username:
            token_dto = TokenDTO(access_token=uuid.UUID(access_token_str), username=username)
            request.person = auth_service.check_access(token_dto)
        response = get_response(request)
        return response

    return middleware
