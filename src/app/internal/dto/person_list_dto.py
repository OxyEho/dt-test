from dataclasses import dataclass


@dataclass
class PersonListDTO:
    username: str
    chosen_usernames: list[str]

    def to_msg(self, msg_begin: str) -> str:
        return f"{msg_begin} {self.username}:\n" + "\n".join(self.chosen_usernames)
