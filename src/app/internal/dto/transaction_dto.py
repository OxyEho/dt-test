import datetime
import enum
import uuid
from dataclasses import dataclass
from decimal import Decimal

from app.internal.models.transaction import Transaction


class TransactionType(str, enum.Enum):
    INCOMING = "incoming"
    OUTGOING = "outgoing"


@dataclass
class TransactionDTO:
    sender_card_num: str
    recipient_card_num: str
    value: Decimal
    transaction_date: datetime.datetime
    transaction_type: TransactionType

    def __str__(self):
        return (
            f"type: {self.transaction_type}; from: {self.sender_card_num} to: {self.recipient_card_num}; "
            f"value: {self.value}; time: {self.transaction_date}."
        )


@dataclass
class TransactionsDTO:
    account_id: uuid.UUID
    transactions: list[TransactionDTO]

    def to_msg(self) -> str:
        account_str = f"account: {self.account_id}\n"
        transactions_str = "\n".join([str(transaction) for transaction in self.transactions])
        return account_str + transactions_str

    @classmethod
    def get_from_transactions(
        cls, account_id: uuid.UUID, incoming_transactions: list[Transaction], outgoing_transactions: list[Transaction]
    ):
        outgoing_transactions_dto = cls.get_transaction_dtos(outgoing_transactions, TransactionType.OUTGOING)
        incoming_transactions_dto = cls.get_transaction_dtos(incoming_transactions, TransactionType.INCOMING)
        return cls(account_id, outgoing_transactions_dto + incoming_transactions_dto)

    @staticmethod
    def get_transaction_dtos(transactions: list[Transaction], transaction_type: TransactionType):
        return [
            TransactionDTO(
                sender_card_num=transaction.sender_card.card_number,
                recipient_card_num=transaction.recipient_card.card_number,
                value=transaction.value,
                transaction_date=transaction.transaction_date,
                transaction_type=transaction_type,
            )
            for transaction in transactions
        ]
