from app.internal.dto.base_dto import BaseDTO


class CardDTO(BaseDTO):
    def __init__(self, person_id: int, card_number: str):
        self.card_number = card_number
        self.person_id = person_id

    @classmethod
    def validate(cls, card_number: str) -> bool:
        return card_number.isnumeric() and len(card_number) == 16
