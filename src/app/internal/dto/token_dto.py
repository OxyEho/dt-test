import uuid
from dataclasses import dataclass


@dataclass
class TokenDTO:
    refresh_token: uuid.UUID = None
    access_token: uuid.UUID = None
    username: str = ""

    def to_dict(self) -> dict:
        return {
            "access_token": self.access_token,
            "refresh_token": self.refresh_token,
            "username": self.username,
        }
