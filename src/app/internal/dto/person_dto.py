import re

from telegram import Update
from telegram.ext import CallbackContext


class ValidationException(Exception):
    pass


class PersonDTO:
    phone_regex = re.compile(r"^\+?1?\d{9,15}$")
    password_regexp = re.compile(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{6,20}$")

    def __init__(self, person_id: int = 0, username: str = "", password: str = None, phone: str = ""):
        self.phone = phone
        self.username = username
        self.person_id = person_id
        self.password = password

    @classmethod
    def validate(cls, phone: str) -> bool:
        return bool(phone and re.match(cls.phone_regex, phone))

    @classmethod
    def validate_password(cls, password: str) -> bool:
        return bool(re.match(cls.password_regexp, password))

    @classmethod
    def get_instance(cls, update: Update):
        return cls(update.effective_user.id, update.effective_user.username)

    def set_phone(self, context: CallbackContext):
        if not context.args or not self.validate(context.args[0]):
            raise ValidationException
        self.phone = context.args[0]

    def set_password(self, context: CallbackContext):
        if not context.args or not self.validate_password(context.args[0]):
            raise ValidationException
        self.password = context.args[0]

    def __hash__(self):
        return self.person_id

    def __eq__(self, other):
        return self.person_id == other.peson_id
