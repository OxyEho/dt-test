from abc import ABC, abstractmethod

from telegram import Update
from telegram.ext import CallbackContext


class BaseDTO(ABC):
    @abstractmethod
    def __init__(self, *args):
        pass

    @classmethod
    def validate(cls, *args) -> bool:
        return True

    @classmethod
    def get_instance(cls, update: Update, context: CallbackContext, *args):
        if context.args:
            if cls.validate(*context.args):
                return cls(update.effective_user.id, *context.args, *args)
        raise ValueError
