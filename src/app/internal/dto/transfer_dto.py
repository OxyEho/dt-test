from dataclasses import dataclass
from decimal import Decimal

from app.internal.dto.person_dto import PersonDTO


@dataclass
class TransferDTO:
    person_dto: PersonDTO
    sender_card: str = None
    value: Decimal = None
    recipient_id: str = None
    recipient_username: str = None
    recipient_card: str = None
