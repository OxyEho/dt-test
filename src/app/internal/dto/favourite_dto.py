import re

from app.internal.dto.base_dto import BaseDTO


class FavouriteDTO(BaseDTO):
    def __init__(self, person_id: int, favourite_username: str):
        self.favourite_username = favourite_username
        self.person_id = person_id

    @classmethod
    def validate(cls, favourite_username: str) -> bool:
        return bool(favourite_username)
