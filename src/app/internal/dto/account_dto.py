from app.internal.dto.base_dto import BaseDTO


class AccountDTO(BaseDTO):
    def __init__(self, person_id: int, account_id: str):
        self.account_id = account_id
        self.person_id = person_id
