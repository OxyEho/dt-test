import enum
import re
import uuid

from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.transfer_dto import TransferDTO
from app.internal.exceptions.transfer_exceptions import (
    CardNotFound,
    DifferentCurrency,
    IncorrectRecipient,
    NotEnoughMoney,
    NotInFavourite,
)
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.favorite import Favorite


class TransferStageEnum(enum.IntEnum):
    SENDER_CARD_ENTERED = 1
    VALUE_ENTERED = 2
    RECIPIENT_ENTERED = 3
    RECIPIENT_CARD_ENTERED = 4


class TransferStage:
    uuid_re = re.compile(r"^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-4[0-9A-Fa-f]{3}-[89AB][0-9A-Fa-f]{3}-[0-9A-Fa-f]{12}$")

    def __init__(self, person_dto: PersonDTO):
        self.stage = TransferStageEnum.SENDER_CARD_ENTERED
        self.transfer_dto = TransferDTO(person_dto=person_dto)

    def validate_sender_card(self):
        exist = Card.objects.filter(
            card_number=self.transfer_dto.sender_card, account__person__username=self.transfer_dto.person_dto.username
        ).exists()
        if not exist:
            raise CardNotFound()

    def validate_value(self):
        card = Card.objects.filter(card_number=self.transfer_dto.sender_card).values("balance").first()
        if card["balance"] < self.transfer_dto.value or self.transfer_dto.value <= 0:
            raise NotEnoughMoney(card["balance"])

    def validate_recipient(self):
        if re.match(self.uuid_re, self.transfer_dto.recipient_id):
            try:
                recipient_uuid = uuid.UUID(self.transfer_dto.recipient_id)
                account = BankAccount.objects.filter(account_id=recipient_uuid).values("person__username").first()
                favourite = Favorite.objects.filter(favourite__username=account["person__username"])
                if not favourite:
                    raise NotInFavourite(account["person__username"])
                self.transfer_dto.recipient_username = account["person__username"]
            except ValueError:
                raise IncorrectRecipient(self.transfer_dto.recipient_id)
        else:
            favourite = Favorite.objects.filter(favourite__username=self.transfer_dto.recipient_id)
            if not favourite:
                raise NotInFavourite(self.transfer_dto.recipient_id)
            self.transfer_dto.recipient_username = self.transfer_dto.recipient_id

    def validate_recipient_card(self):
        sender_card = (
            Card.objects.filter(
                card_number=self.transfer_dto.sender_card,
                account__person__username=self.transfer_dto.person_dto.username,
            )
            .values("currency")
            .first()
        )
        recipient_card = (
            Card.objects.filter(
                card_number=self.transfer_dto.recipient_card,
                account__person__username=self.transfer_dto.recipient_username,
            )
            .values("currency")
            .first()
        )
        if not recipient_card:
            raise CardNotFound()
        if sender_card["currency"] != recipient_card["currency"]:
            raise DifferentCurrency()

    def next_stage(self):
        stage = self.stage + 1
        self.stage = TransferStageEnum(stage)
