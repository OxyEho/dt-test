import uuid

from django.db import models
from django.utils.timezone import now

from app.internal.models.person import Person


class BankAccount(models.Model):
    account_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    owner_name = models.CharField(max_length=43, null=False)
    owner_last_name = models.CharField(max_length=43, null=False)
    patronymic = models.CharField(max_length=46, null=True)
    open_date = models.DateTimeField(null=False, default=now)

    def __str__(self):
        return str(self.account_id)
