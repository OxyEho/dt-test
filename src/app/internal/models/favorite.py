from django.db import models

from app.internal.models.person import Person


class Favorite(models.Model):
    owner = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="owner")
    favourite = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="favourite")

    class Meta:
        unique_together = (("owner", "favourite"),)

    def __str__(self):
        return f"{self.owner.username}: {self.favourite.username}"
