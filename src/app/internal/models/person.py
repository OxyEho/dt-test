from django.core.validators import RegexValidator
from django.db import models

PHONE_REGEX = RegexValidator(regex=r"^\+?1?\d{9,15}$", message="Incorrect phone format")


class Person(models.Model):
    person_id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=255, null=True, unique=True)
    phone = models.CharField(validators=[PHONE_REGEX], max_length=17, blank=True)
    hash_password = models.CharField(max_length=255, null=True, editable=False)

    def to_dict(self) -> dict:
        return {"id": self.person_id, "name": self.username, "phone": self.phone}

    def __str__(self):
        return self.username
