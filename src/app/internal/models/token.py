from django.db import models
from django.utils import timezone

from app.internal.models.person import Person


class Token(models.Model):
    access_token = models.UUIDField(unique=True, editable=False)
    refresh_token = models.UUIDField(unique=True, editable=False)
    person = models.OneToOneField(Person, on_delete=models.CASCADE, null=False, editable=False, primary_key=True)
    creation_time = models.DateTimeField(null=False, editable=False, default=timezone.now)
    is_useful = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.person.username}"
