from django.db import models
from django.utils.timezone import now

from app.internal.models.card import Card


class Transaction(models.Model):
    sender_card = models.ForeignKey(Card, on_delete=models.DO_NOTHING, editable=False, related_name="sender_cards")
    recipient_card = models.ForeignKey(
        Card, on_delete=models.DO_NOTHING, editable=False, related_name="recipient_cards"
    )
    value = models.DecimalField(max_digits=20, decimal_places=2, editable=False)
    transaction_date = models.DateTimeField(null=False, default=now, editable=False)

    class Meta:
        unique_together = (("sender_card", "recipient_card", "transaction_date"),)

    def __str__(self):
        return f"{self.transaction_date}\n{self.sender_card} to {self.recipient_card}"
