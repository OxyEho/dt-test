import uuid

from django.db import models
from django.utils.timezone import now, timedelta

from app.internal.models.bank_account import BankAccount


def get_expired_date():
    return now() + timedelta(days=365)


def generate_card_id() -> str:
    pre_id = str(uuid.uuid4()).replace("-", "")
    id_ = int(pre_id, 16)
    while id_ > pow(10, 15):
        id_ //= 10
    id_ = id_ * 10 + luhn_num(str(id_))
    return str(id_)


def luhn_num(str_num: str) -> int:
    sum_ = sum([int(digit) for digit in str_num[1::2]])
    sum_ += sum([int(digit) * 2 if int(digit) * 2 <= 9 else int(digit) * 2 - 9 for digit in str_num[::2]])
    last_num = 10 - (sum_ % 10)
    last_num = last_num if last_num != 10 else 0
    return last_num


class Card(models.Model):
    card_number = models.CharField(primary_key=True, default=generate_card_id, editable=False, max_length=16)
    account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    open_date = models.DateTimeField(null=False, default=now)
    expired_date = models.DateTimeField(null=False, default=get_expired_date)
    currency = models.CharField(max_length=3, null=False)
    balance = models.DecimalField(max_digits=20, decimal_places=2)

    def display_account(self):
        return self.account.account_id

    display_account.short_description = "account"

    def __str__(self):
        return self.card_number

    def msg_str(self):
        return f"Card: {self.card_number}\n{self.currency}: {self.balance}"
