from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.models.admin_user import AdminUser
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.favorite import Favorite
from app.internal.models.person import Person
from app.internal.models.token import Token
from app.internal.models.transaction import Transaction


class CardInline(admin.TabularInline):
    model = Card


@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    pass


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ("person_id", "username", "phone")


@admin.register(BankAccount)
class BankAccountAdmin(admin.ModelAdmin):
    list_display = ("account_id", "owner_name", "owner_last_name", "patronymic", "open_date")
    inlines = [
        CardInline,
    ]


@admin.register(Card)
class CardModelAdmin(admin.ModelAdmin):
    list_display = ("card_number", "display_account", "open_date", "expired_date", "currency", "balance")


@admin.register(Favorite)
class FavoritesAdmin(admin.ModelAdmin):
    list_display = ("owner", "favourite")


@admin.register(Token)
class TokenAdmin(admin.ModelAdmin):
    list_display = ("access_token", "refresh_token", "creation_time", "is_useful")


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ("sender_card", "recipient_card", "value", "transaction_date")
