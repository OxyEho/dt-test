from queue import Queue

from django.conf import settings
from telegram import Bot, Update
from telegram.ext import CommandHandler, Dispatcher, Filters, MessageHandler, Updater

import app.internal.transport.bot.handlers as handlers


def _setup_handlers(dispatcher: Dispatcher) -> Dispatcher:
    dispatcher.add_handler(CommandHandler("start", handlers.start))
    dispatcher.add_handler(CommandHandler("set_phone", handlers.set_phone))
    dispatcher.add_handler(CommandHandler("me", handlers.me))
    dispatcher.add_handler(CommandHandler("card", handlers.card_balance))
    dispatcher.add_handler(CommandHandler("account", handlers.account_balance))
    dispatcher.add_handler(CommandHandler("help", handlers.help_))
    dispatcher.add_handler(CommandHandler("add_favourite", handlers.add_favourite))
    dispatcher.add_handler(CommandHandler("delete_favourite", handlers.delete_favourite))
    dispatcher.add_handler(CommandHandler("show_favourite", handlers.show_favourites))
    dispatcher.add_handler(CommandHandler("send_money", handlers.send_money))
    dispatcher.add_handler(CommandHandler("cancel", handlers.cancel_transfer))
    dispatcher.add_handler(CommandHandler("extract", handlers.extract))
    dispatcher.add_handler(CommandHandler("activity", handlers.activity))
    dispatcher.add_handler(CommandHandler("set_password", handlers.set_password))
    dispatcher.add_handler(MessageHandler(Filters.text & (~Filters.command), handlers.transfer))
    return dispatcher


class Singleton:
    instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance


class WebhookBot(Singleton):
    def __init__(self):
        self.bot = Bot(settings.TG_TOKEN)
        self.dispatcher = _setup_handlers(Dispatcher(self.bot, update_queue=Queue(), use_context=True))

    def process_update(self, json_update):
        self.dispatcher.process_update(Update.de_json(json_update, self.bot))


class PollingBot(Singleton):
    def __init__(self):
        self.updater = Updater(settings.TG_TOKEN)
        self.dispatcher = _setup_handlers(self.updater.dispatcher)

    def run(self):
        self.updater.start_polling()
        self.updater.idle()
