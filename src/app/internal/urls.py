from django.urls import path

import app.internal.transport.rest.handlers as handlers

urlpatterns = [path("me/<int:pk>", handlers.me), path("login", handlers.login), path("refresh", handlers.refresh)]
