from decimal import Decimal

from django.db import transaction
from django.db.models import Q

from app.internal.bot_stages.transfer_stage import TransferStage, TransferStageEnum
from app.internal.dto.account_dto import AccountDTO
from app.internal.dto.card_dto import CardDTO
from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.person_list_dto import PersonListDTO
from app.internal.dto.transaction_dto import TransactionsDTO
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.transaction import Transaction


class BankService:
    @staticmethod
    def get_card(card_dto: CardDTO) -> Card:
        return Card.objects.get(card_number=card_dto.card_number, account__person_id=card_dto.person_id)

    @staticmethod
    def get_account_cards(account_dto: AccountDTO) -> list[Card]:
        return Card.objects.filter(account__account_id=account_dto.account_id, account__person_id=account_dto.person_id)

    @classmethod
    def update_transfer(cls, transfer_stage: TransferStage, msg: str):
        match transfer_stage.stage:
            case TransferStageEnum.SENDER_CARD_ENTERED:
                transfer_stage.transfer_dto.sender_card = msg
                transfer_stage.validate_sender_card()
            case TransferStageEnum.VALUE_ENTERED:
                transfer_stage.transfer_dto.value = Decimal(msg)
                transfer_stage.validate_value()
            case TransferStageEnum.RECIPIENT_ENTERED:
                transfer_stage.transfer_dto.recipient_id = msg
                transfer_stage.validate_recipient()
            case TransferStageEnum.RECIPIENT_CARD_ENTERED:
                transfer_stage.transfer_dto.recipient_card = msg
                transfer_stage.validate_recipient_card()
                cls.transfer_money(transfer_stage)

    @staticmethod
    def transfer_money(transfer_stage: TransferStage):
        with transaction.atomic():
            sender_card = Card.objects.filter(card_number=transfer_stage.transfer_dto.sender_card).first()
            recipient_card = Card.objects.filter(card_number=transfer_stage.transfer_dto.recipient_card).first()
            sender_card.balance -= transfer_stage.transfer_dto.value
            recipient_card.balance += transfer_stage.transfer_dto.value
            sender_card.save(update_fields=("balance",))
            recipient_card.save(update_fields=("balance",))
            Transaction.objects.create(
                sender_card=sender_card, recipient_card=recipient_card, value=transfer_stage.transfer_dto.value
            )

    @staticmethod
    def get_account_extract(person_dto: PersonDTO) -> TransactionsDTO:
        with transaction.atomic():
            account_id = (
                BankAccount.objects.filter(person_id=person_dto.person_id).values("account_id").first()["account_id"]
            )
            person_card_nums = Card.objects.filter(account__account_id=account_id).values_list("card_number", flat=True)
            outgoing_transactions = Transaction.objects.filter(sender_card__in=person_card_nums).select_related(
                "sender_card", "recipient_card"
            )
            incoming_transactions = Transaction.objects.filter(recipient_card__in=person_card_nums).select_related(
                "sender_card", "recipient_card"
            )
            return TransactionsDTO.get_from_transactions(account_id, incoming_transactions, outgoing_transactions)

    @staticmethod
    def get_activity_persons(person_dto: PersonDTO) -> PersonListDTO:
        with transaction.atomic():
            person_card_nums = Card.objects.filter(account__person_id=person_dto.person_id).values_list(
                "card_number", flat=True
            )
            username_pairs = (
                Transaction.objects.filter(Q(sender_card__in=person_card_nums) | Q(recipient_card__in=person_card_nums))
                .values("sender_card__account__person__username", "recipient_card__account__person__username")
                .distinct()
            )
            activity_persons = [
                pair["sender_card__account__person__username"]
                if pair["sender_card__account__person__username"] != person_dto.username
                else pair["recipient_card__account__person__username"]
                for pair in username_pairs
            ]
            return PersonListDTO(person_dto.username, activity_persons)
