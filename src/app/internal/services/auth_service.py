import datetime
import uuid

from django.contrib.auth.hashers import make_password
from django.db import transaction
from django.utils import timezone

from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.token_dto import TokenDTO
from app.internal.exceptions.auth_exeptions import IncorrectPassword, IncorrectToken, RefreshExpired
from app.internal.exceptions.person_exceptions import PersonNotFound
from app.internal.models.person import Person
from app.internal.models.token import Token
from config.settings import PASSWORD_SALT

ACCESS_EXPIRED = datetime.timedelta(minutes=10)
REFRESH_EXPIRED = datetime.timedelta(days=1)


def check_access(token_dto: TokenDTO) -> Person | None:
    token = (
        Token.objects.filter(access_token=token_dto.access_token, person__username=token_dto.username)
        .select_related("person")
        .first()
    )
    if not token:
        return None
    if token.creation_time + ACCESS_EXPIRED < timezone.now() or not token.is_useful:
        return None
    return token.person


def refresh_access(token_dto: TokenDTO) -> TokenDTO:
    refresh_time = timezone.now()
    token = Token.objects.filter(refresh_token=token_dto.refresh_token).select_related("person").first()
    if not token:
        raise IncorrectToken
    if token.creation_time + REFRESH_EXPIRED < refresh_time:
        token.is_useful = False
        token.save(update_fields=["is_useful"])
        raise RefreshExpired
    return _refresh_tokens(refresh_time, token)


def login(person_dto: PersonDTO) -> TokenDTO:
    person = Person.objects.filter(username=person_dto.username).first()
    if not person:
        raise PersonNotFound
    hash_password = make_password(person_dto.password, salt=PASSWORD_SALT, hasher="pbkdf2_sha256")
    if hash_password != person.hash_password:
        raise IncorrectPassword
    token = Token.objects.filter(person__username=person_dto.username).select_related("person").first()
    if not token:
        token = Token()
        token.person = person
    return _refresh_tokens(token=token)


def set_password(person_dto: PersonDTO):
    person = Person.objects.filter(person_id=person_dto.person_id).first()
    person.hash_password = make_password(person_dto.password, salt=PASSWORD_SALT, hasher="pbkdf2_sha256")
    person.save(update_fields=["hash_password"])


def _refresh_tokens(refresh_time=timezone.now(), token: Token = None) -> TokenDTO:
    token.access_token = uuid.uuid4()
    token.refresh_token = uuid.uuid4()
    token.creation_time = refresh_time
    token.is_useful = True
    with transaction.atomic():
        token.save()
    return TokenDTO(access_token=token.access_token, refresh_token=token.refresh_token, username=token.person.username)
