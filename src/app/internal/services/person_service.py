from app.internal.dto.favourite_dto import FavouriteDTO
from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.person_list_dto import PersonListDTO
from app.internal.exceptions.favourite_exceptions import FavouriteNotFound
from app.internal.exceptions.person_exceptions import PersonNotFound
from app.internal.models.favorite import Favorite
from app.internal.models.person import Person


class PersonService:
    @staticmethod
    def save(person_dto: PersonDTO):
        person = Person()
        person.person_id = person_dto.person_id
        person.username = person_dto.username
        if person_dto.phone:
            person.phone = person_dto.phone
        person.save()

    @staticmethod
    def update(person_dto: PersonDTO):
        person = Person.objects.filter(person_id=person_dto.person_id).first()
        person.phone = person_dto.phone
        person.save(update_fields=("phone",))

    @staticmethod
    def add_favourite(favourite_dto: FavouriteDTO):
        owner_person = Person.objects.filter(person_id=favourite_dto.person_id).first()
        favourite_person = Person.objects.filter(username=favourite_dto.favourite_username).first()
        if favourite_person:
            favourite = Favorite()
            favourite.owner = owner_person
            favourite.favourite = favourite_person
            favourite.save()
            return
        raise PersonNotFound(favourite_dto.favourite_username)

    @staticmethod
    def delete_favourite(favourite_dto: FavouriteDTO):
        owner_person = Person.objects.filter(person_id=favourite_dto.person_id).values("username").first()
        favourite_person = Person.objects.filter(username=favourite_dto.favourite_username).values("username").first()
        if favourite_person:
            favourite = Favorite.objects.filter(
                owner__username=owner_person["username"], favourite__username=favourite_person["username"]
            ).first()
            if favourite:
                favourite.delete()
                return
            raise FavouriteNotFound(
                owner_username=owner_person["username"], favourite_username=favourite_person["username"]
            )
        raise PersonNotFound(favourite_dto.favourite_username)

    @staticmethod
    def get_favourites(person_dto: PersonDTO) -> PersonListDTO:
        favourite_usernames = Favorite.objects.filter(owner__person_id=person_dto.person_id).values(
            "favourite__username"
        )
        favourites = [favourite_username["favourite__username"] for favourite_username in favourite_usernames]
        return PersonListDTO(person_dto.username, favourites)
