from django.core.management.base import BaseCommand

from app.internal.bot import PollingBot


class Command(BaseCommand):
    def handle(self, *args, **options):
        PollingBot().run()
