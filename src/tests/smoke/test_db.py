import pytest

from app.internal.models.person import Person


@pytest.mark.smoke
@pytest.mark.django_db
def test_db():
    Person.objects.create(person_id=1, username="test")
