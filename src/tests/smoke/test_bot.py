import pytest

from app.internal.bot import WebhookBot


@pytest.mark.smoke
def test_bot():
    bot = WebhookBot()
    bot.process_update({})
