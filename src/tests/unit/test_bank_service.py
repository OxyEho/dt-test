from decimal import Decimal

import pytest

from app.internal.bot_stages.transfer_stage import TransferStageEnum
from app.internal.exceptions.transfer_exceptions import CardNotFound, NotEnoughMoney, NotInFavourite
from app.internal.models.card import Card
from app.internal.services.bank_service import BankService


class TestBankService:
    @pytest.mark.django_db
    @pytest.mark.unit
    def test_get_card(self, test_person_with_account_card, test_card_dto):
        _, _, expected_card = test_person_with_account_card
        actual_card = BankService.get_card(test_card_dto)
        assert actual_card == expected_card

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_get_account_cards(self, test_person_with_account_card, test_account_dto):
        person, account, card = test_person_with_account_card
        expected_cards = [card]
        actual_cards = BankService.get_account_cards(test_account_dto)
        assert expected_cards == list(actual_cards)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_sender_entered(self, test_transfer_stage):
        sender_card = test_transfer_stage.transfer_dto.sender_card
        BankService.update_transfer(test_transfer_stage, sender_card)
        assert test_transfer_stage.transfer_dto.sender_card == sender_card

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_value_entered(self, test_transfer_stage):
        test_transfer_stage.stage = TransferStageEnum.VALUE_ENTERED
        value = "1"
        BankService.update_transfer(test_transfer_stage, value)
        assert test_transfer_stage.transfer_dto.value == Decimal(value)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_recipient_entered(self, test_transfer_stage):
        test_transfer_stage.stage = TransferStageEnum.RECIPIENT_ENTERED
        recipient_id = test_transfer_stage.transfer_dto.recipient_id
        BankService.update_transfer(test_transfer_stage, recipient_id)
        assert test_transfer_stage.transfer_dto.recipient_username == recipient_id

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_recipient_card_entered(self, test_transfer_stage):
        old_sender_card = Card.objects.get(card_number=test_transfer_stage.transfer_dto.sender_card)
        old_recipient_card = Card.objects.get(card_number=test_transfer_stage.transfer_dto.recipient_card)
        test_transfer_stage.stage = TransferStageEnum.RECIPIENT_CARD_ENTERED
        recipient_card_num = test_transfer_stage.transfer_dto.recipient_card
        BankService.update_transfer(test_transfer_stage, recipient_card_num)
        assert test_transfer_stage.transfer_dto.recipient_card == recipient_card_num
        new_sender_card = Card.objects.get(card_number=test_transfer_stage.transfer_dto.sender_card)
        new_recipient_card = Card.objects.get(card_number=test_transfer_stage.transfer_dto.recipient_card)
        new_recipient_balance = new_recipient_card.balance
        old_recipient_balance = old_recipient_card.balance
        new_sender_balance = new_sender_card.balance
        old_sender_balance = old_sender_card.balance
        assert old_sender_balance - new_sender_balance == Decimal(test_transfer_stage.transfer_dto.value)
        assert new_recipient_balance - old_recipient_balance == Decimal(test_transfer_stage.transfer_dto.value)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_sender_entered_incorrect_sender_card(self, test_transfer_stage):
        sender_card = "1" * 16
        with pytest.raises(CardNotFound):
            BankService.update_transfer(test_transfer_stage, sender_card)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_value_entered_incorrect_value(self, test_transfer_stage):
        value = "-100"
        test_transfer_stage.stage = TransferStageEnum.VALUE_ENTERED
        with pytest.raises(NotEnoughMoney):
            BankService.update_transfer(test_transfer_stage, value)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_recipient_entered_incorrect_recipient(self, test_transfer_stage):
        non_existing_username = "non_exists"
        test_transfer_stage.stage = TransferStageEnum.RECIPIENT_ENTERED
        with pytest.raises(NotInFavourite):
            BankService.update_transfer(test_transfer_stage, non_existing_username)
        non_existing_account = "11111111-1111-1111-1111-111111111111"
        with pytest.raises(NotInFavourite):
            BankService.update_transfer(test_transfer_stage, non_existing_account)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_update_transfer_recipient_card_entered_incorrect_value(self, test_transfer_stage):
        recipient_card = "1" * 16
        test_transfer_stage.stage = TransferStageEnum.RECIPIENT_CARD_ENTERED
        with pytest.raises(CardNotFound):
            BankService.update_transfer(test_transfer_stage, recipient_card)

    @pytest.mark.django_db
    @pytest.mark.unit
    @pytest.mark.parametrize("test_persons_with_transaction", [(1, "first", 2, "second")], indirect=True)
    def test_get_activity_persons(self, test_persons_with_transaction):
        first_person_dto, second_person_dto = test_persons_with_transaction
        person_list_dto = BankService.get_activity_persons(first_person_dto)
        assert person_list_dto.chosen_usernames == [second_person_dto.username]
        assert person_list_dto.username == first_person_dto.username

    @pytest.mark.django_db
    @pytest.mark.unit
    @pytest.mark.parametrize("test_persons_with_transaction", [(1, "first", 2, "second")], indirect=True)
    def test_get_activity_persons_incorrect_usernames(self, test_persons_with_transaction):
        first_person_dto, second_person_dto = test_persons_with_transaction
        person_list_dto = BankService.get_activity_persons(first_person_dto)
        assert person_list_dto.chosen_usernames != []
        assert person_list_dto.chosen_usernames != ["not exists"]
        assert person_list_dto.username == first_person_dto.username
