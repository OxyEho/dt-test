import pytest

from app.internal.dto.card_dto import CardDTO
from app.internal.dto.favourite_dto import FavouriteDTO
from app.internal.dto.person_dto import PersonDTO, ValidationException


class TestDTO:
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("77777777777",))], indirect=True)
    @pytest.mark.unit
    def test_get_person_dto_with_correct_phone(self, test_update_context):
        mock_update, mock_context = test_update_context
        person_dto = PersonDTO.get_instance(mock_update)
        person_dto.set_phone(mock_context)
        assert person_dto.person_id == 1
        assert person_dto.username == "test_username"
        assert person_dto.phone == "77777777777"

    @pytest.mark.parametrize(
        "test_update_context",
        [(1, "test_username", ("sadasds",)), (1, "test_username", ("11111",)), (1, "test_username", ("",))],
        indirect=True,
    )
    @pytest.mark.unit
    def test_get_person_dto_with_incorrect_phone(self, test_update_context):
        mock_update, mock_context = test_update_context
        person_dto = PersonDTO.get_instance(mock_update)
        with pytest.raises(ValidationException):
            person_dto.set_phone(mock_context)

    @pytest.mark.parametrize("card_num", ["1589462896557484", "2382810694475373"])
    @pytest.mark.unit
    def test_validate_correct_card(self, card_num):
        assert CardDTO.validate(card_num)

    @pytest.mark.parametrize("card_num", ["aaaaaaaaaaaaaaaa", "1", ""])
    @pytest.mark.unit
    def test_validate_incorrect_card(self, card_num):
        assert not CardDTO.validate(card_num)

    @pytest.mark.parametrize("favourite_username", ["admin", "test", "correct"])
    @pytest.mark.unit
    def test_validate_correct_favourite(self, favourite_username):
        assert FavouriteDTO.validate(favourite_username)

    @pytest.mark.parametrize("favourite_username", [""])
    @pytest.mark.unit
    def test_validate_incorrect_favourite(self, favourite_username):
        assert not FavouriteDTO.validate(favourite_username)

    @pytest.mark.parametrize("password", ["aaa", "BbB", "123"])
    @pytest.mark.unit
    def test_password_failed_validation(self, password):
        assert not PersonDTO.validate_password(password)

    @pytest.mark.parametrize("password", ["Qwr@tY4!", "ZVCBc*&1"])
    @pytest.mark.unit
    def test_password_validation(self, password):
        assert PersonDTO.validate_password(password)
