import pytest

from app.internal.dto.favourite_dto import FavouriteDTO
from app.internal.dto.person_dto import PersonDTO
from app.internal.exceptions.favourite_exceptions import FavouriteNotFound
from app.internal.exceptions.person_exceptions import PersonNotFound
from app.internal.models.favorite import Favorite
from app.internal.models.person import Person
from app.internal.services.person_service import PersonService


class TestPersonService:
    @pytest.mark.django_db
    @pytest.mark.unit
    @pytest.mark.parametrize(
        "person_dto",
        [PersonDTO(person_id=100, username="test_one", phone="7" * 11), PersonDTO(person_id=200, username="test_two")],
    )
    def test_save_person(self, person_dto):
        PersonService.save(person_dto)
        person = Person.objects.get(person_id=person_dto.person_id)
        assert person.person_id == person_dto.person_id
        assert person.username == person_dto.username
        assert person.phone == person_dto.phone

    @pytest.mark.django_db
    @pytest.mark.unit
    @pytest.mark.parametrize(
        "person_dto",
        [
            PersonDTO(person_id=100, username="test_one", phone="7" * 11),
            PersonDTO(person_id=200, username="test_two", phone="7" + "1" * 10),
        ],
    )
    def test_update_person(self, person_dto):
        PersonService.save(person_dto)
        new_phone = "70000000000"
        person_dto.phone = new_phone
        PersonService.update(person_dto)
        person = Person.objects.get(person_id=person_dto.person_id)
        assert person.phone == new_phone

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_add_favorite(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        PersonService.save(person_two)
        favourite_dto = FavouriteDTO(person_id=person_one.person_id, favourite_username=person_two.username)
        PersonService.add_favourite(favourite_dto)
        favourite_pair = Favorite.objects.get(
            owner__person_id=person_one.person_id, favourite__person_id=person_two.person_id
        )
        assert favourite_pair.owner.person_id == person_one.person_id
        assert favourite_pair.favourite.username == person_two.username

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_add_favorite_non_existing_person(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        favourite_dto = FavouriteDTO(person_id=person_one.person_id, favourite_username=person_two.username)
        with pytest.raises(PersonNotFound):
            PersonService.add_favourite(favourite_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_delete_favorite(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        PersonService.save(person_two)
        Favorite.objects.create(owner_id=person_one.person_id, favourite_id=person_two.person_id)
        favourite_dto = FavouriteDTO(person_id=person_one.person_id, favourite_username=person_two.username)
        PersonService.delete_favourite(favourite_dto)
        non_existing_favourite = Favorite.objects.filter(
            owner_id=person_one.person_id, favourite_id=person_two.person_id
        ).first()
        assert not non_existing_favourite

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_delete_favorite_non_existing_person(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        favourite_dto = FavouriteDTO(person_id=person_one.person_id, favourite_username=person_two.username)
        with pytest.raises(PersonNotFound):
            PersonService.delete_favourite(favourite_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_delete_favorite_non_existing_favourite(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        PersonService.save(person_two)
        favourite_dto = FavouriteDTO(person_id=person_one.person_id, favourite_username=person_two.username)
        with pytest.raises(FavouriteNotFound):
            PersonService.delete_favourite(favourite_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_get_favorites(self, test_two_person_dto):
        person_one, person_two = test_two_person_dto
        PersonService.save(person_one)
        PersonService.save(person_two)
        Favorite.objects.create(owner_id=person_one.person_id, favourite_id=person_two.person_id)
        favourites_dto = PersonService.get_favourites(person_one)
        assert favourites_dto.chosen_usernames == [person_two.username]

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_get_favorites_no_favourites(self):
        person_one = PersonDTO(person_id=100, username="test_one")
        PersonService.save(person_one)
        favourites_dto = PersonService.get_favourites(person_one)
        assert favourites_dto.chosen_usernames == []
