import uuid

import pytest
from django.contrib.auth.hashers import make_password

import app.internal.services.auth_service as auth_service
from app.internal.dto.person_dto import PersonDTO
from app.internal.dto.token_dto import TokenDTO
from app.internal.exceptions.auth_exeptions import IncorrectToken, RefreshExpired
from app.internal.models.person import Person
from config.settings import PASSWORD_SALT


class TestAuthService:
    @pytest.mark.django_db
    @pytest.mark.unit
    def test_refresh_token(self, test_token):
        token_dto = TokenDTO(test_token.refresh_token, test_token.access_token, test_token.person.username)
        token_dto = auth_service.refresh_access(token_dto)
        assert token_dto.access_token != test_token.access_token
        assert token_dto.refresh_token != test_token.refresh_token
        assert token_dto.username == test_token.person.username

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_expired_refresh_token(self, test_expired_token):
        token_dto = TokenDTO(
            test_expired_token.refresh_token, test_expired_token.access_token, test_expired_token.person.username
        )
        with pytest.raises(RefreshExpired):
            auth_service.refresh_access(token_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_incorrect_refresh_token(self, test_token):
        incorrect_refresh = uuid.uuid4()
        token_dto = TokenDTO(incorrect_refresh, test_token.access_token, test_token.person.username)
        with pytest.raises(IncorrectToken):
            auth_service.refresh_access(token_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_set_password(self):
        password = "test_password"
        person = Person.objects.create(person_id=1, username="test")
        person_dto = PersonDTO(person_id=person.person_id, username="test", password=password)
        auth_service.set_password(person_dto)
        person = Person.objects.get(person_id=1)
        assert make_password(password, salt=PASSWORD_SALT, hasher="pbkdf2_sha256") == person.hash_password

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_check_access(self, test_token):
        token_dto = TokenDTO(test_token.refresh_token, test_token.access_token, test_token.person.username)
        auth_service.check_access(token_dto=token_dto)

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_expire_check_access(self, test_expired_token):
        token_dto = TokenDTO(
            test_expired_token.refresh_token, test_expired_token.access_token, test_expired_token.person.username
        )
        assert auth_service.check_access(token_dto=token_dto) is None

    @pytest.mark.django_db
    @pytest.mark.unit
    def test_incorrect_person_check_access(self, test_token):
        token_dto = TokenDTO(test_token.refresh_token, test_token.access_token, "incorrect")
        assert auth_service.check_access(token_dto=token_dto) is None
