import uuid
from decimal import Decimal

import pytest
from django.utils import timezone
from django.utils.timezone import now

from app.internal.bot_stages.transfer_stage import TransferStage
from app.internal.dto.account_dto import AccountDTO
from app.internal.dto.card_dto import CardDTO
from app.internal.dto.person_dto import PersonDTO
from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.favorite import Favorite
from app.internal.models.person import Person
from app.internal.models.token import Token
from app.internal.models.transaction import Transaction
from app.internal.services import auth_service


@pytest.fixture(scope="function")
def test_card_dto(test_person_with_account_card):
    person, _, card = test_person_with_account_card
    return CardDTO(person.person_id, card.card_number)


@pytest.fixture(scope="function")
def test_account_dto(test_person_with_account_card):
    person, account, _ = test_person_with_account_card
    return AccountDTO(person.person_id, account.account_id)


@pytest.fixture(scope="function")
def test_persons_to_transfer(
    first_person_id=1,
    first_username="test1",
    first_phone="77777777777",
    first_card_num="4216807515054906",
    second_person_id=2,
    second_username="test2",
    second_phone="71111111111",
    second_card_num="1452799236842967",
):
    first_person = Person.objects.create(person_id=first_person_id, username=first_username, phone=first_phone)
    first_bank_account = BankAccount.objects.create(
        account_id=uuid.uuid4(),
        person=first_person,
        owner_name="test",
        owner_last_name="test",
        patronymic="test",
        open_date=now(),
    )
    first_card = Card.objects.create(
        card_number=first_card_num, account=first_bank_account, open_date=now(), currency="RUB", balance=1000
    )

    second_person = Person.objects.create(person_id=second_person_id, username=second_username, phone=second_phone)
    second_bank_account = BankAccount.objects.create(
        account_id=uuid.uuid4(),
        person=second_person,
        owner_name="test",
        owner_last_name="test",
        patronymic="test",
        open_date=now(),
    )
    second_card = Card.objects.create(
        card_number=second_card_num, account=second_bank_account, open_date=now(), currency="RUB", balance=0
    )

    Favorite.objects.create(owner=first_person, favourite=second_person)
    return first_person, first_bank_account, first_card, second_person, second_bank_account, second_card


@pytest.fixture(scope="function")
def test_transfer_stage(test_persons_to_transfer):
    first_person, first_account, first_card, second_person, second_account, second_card = test_persons_to_transfer
    sender_person_dto = PersonDTO(first_person.username, first_person.username, first_person.phone)
    transfer_stage = TransferStage(sender_person_dto)
    transfer_stage.transfer_dto.sender_card = first_card.card_number
    transfer_stage.transfer_dto.recipient_id = second_person.username
    transfer_stage.transfer_dto.value = Decimal(1)
    transfer_stage.transfer_dto.recipient_username = second_person.username
    transfer_stage.transfer_dto.recipient_card = second_card.card_number
    return transfer_stage


@pytest.fixture(scope="function")
def test_two_person_dto():
    person_one = PersonDTO(person_id=100, username="test_one")
    person_two = PersonDTO(person_id=200, username="test_two")
    return person_one, person_two


@pytest.fixture(scope="function")
def test_persons_with_transaction(request):
    first_person_id, first_username, second_person_id, second_username = request.param
    first_person = Person.objects.create(person_id=first_person_id, username=first_username)
    first_account = BankAccount.objects.create(person=first_person, owner_name="a", owner_last_name="a", patronymic="a")
    first_card = Card.objects.create(account=first_account, currency="RUB", balance=Decimal(1000))
    second_person = Person.objects.create(person_id=second_person_id, username=second_username)
    second_account = BankAccount.objects.create(
        person=second_person, owner_name="a", owner_last_name="a", patronymic="a"
    )
    second_card = Card.objects.create(account=second_account, currency="RUB", balance=Decimal(1000))
    Transaction.objects.create(sender_card=first_card, recipient_card=second_card, value=Decimal(10))
    return PersonDTO(person_id=first_person_id, username=first_username), PersonDTO(
        person_id=second_person, username=second_username
    )
