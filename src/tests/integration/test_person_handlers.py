import pytest

from app.internal.dto.person_list_dto import PersonListDTO
from app.internal.transport.bot.handlers import (
    BotMsg,
    add_favourite,
    delete_favourite,
    me,
    set_phone,
    show_favourites,
    start,
)


class TestPersonHandlers:
    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("77777777777",))], indirect=True)
    def test_start(self, test_update_context):
        mock_update, mock_context = test_update_context
        start(mock_update, mock_context)
        assert mock_context.answer == BotMsg.get_hello("test_username")

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("77777777777",))], indirect=True)
    @pytest.mark.parametrize("test_person", [(1, "test_username", "77777777777")], indirect=True)
    def test_me_with_phone(self, test_update_context, test_person):
        mock_update, mock_context = test_update_context
        me(mock_update, mock_context)
        assert mock_context.answer == BotMsg.get_person_info_msg(test_person)

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", [])], indirect=True)
    @pytest.mark.parametrize("test_person", [(1, "test_username", "")], indirect=True)
    def test_me_without_phone(self, test_update_context, test_person):
        mock_update, mock_context = test_update_context
        me(mock_update, mock_context)
        assert mock_context.answer == BotMsg.NO_ME_PHONE

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("77777777777",))], indirect=True)
    @pytest.mark.parametrize("test_person", [(1, "test_username", "")], indirect=True)
    def test_set_phone_correct(self, test_update_context, test_person):
        mock_update, mock_context = test_update_context
        set_phone(mock_update, mock_context)
        assert mock_context.answer == BotMsg.SAVE_PHONE

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", ("test2",))], indirect=True)
    @pytest.mark.parametrize("test_persons_not_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_add_favourite_correct(self, test_update_context, test_persons_not_add_favourite):
        mock_update, mock_context = test_update_context
        add_favourite(mock_update, mock_context)
        assert mock_context.answer == BotMsg.FAVOURITE_ADDED

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", ("non_existing_username",))], indirect=True)
    @pytest.mark.parametrize("test_persons_not_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_add_favourite_incorrect(self, test_update_context, test_persons_not_add_favourite):
        mock_update, mock_context = test_update_context
        add_favourite(mock_update, mock_context)
        assert mock_context.answer == BotMsg.person_not_found("non_existing_username")

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", ("test2",))], indirect=True)
    @pytest.mark.parametrize("test_persons_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_delete_favourite_correct(self, test_update_context, test_persons_add_favourite):
        mock_update, mock_context = test_update_context
        delete_favourite(mock_update, mock_context)
        assert mock_context.answer == BotMsg.FAVOURITE_DELETED

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", ("test2",))], indirect=True)
    @pytest.mark.parametrize("test_persons_not_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_delete_not_in_favourite(self, test_update_context, test_persons_not_add_favourite):
        owner, favourite = test_persons_not_add_favourite
        mock_update, mock_context = test_update_context
        delete_favourite(mock_update, mock_context)
        assert mock_context.answer == BotMsg.favourite_not_found(owner.username, favourite.username)

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", ("non_existing_username",))], indirect=True)
    @pytest.mark.parametrize("test_persons_not_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_favourite_delete_non_existing_person(self, test_update_context, test_persons_not_add_favourite):
        mock_update, mock_context = test_update_context
        delete_favourite(mock_update, mock_context)
        assert mock_context.answer == BotMsg.person_not_found("non_existing_username")

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test1", [])], indirect=True)
    @pytest.mark.parametrize("test_persons_add_favourite", [(1, "test1", 2, "test2")], indirect=True)
    def test_show_favourites(self, test_update_context, test_persons_add_favourite):
        owner, favourite = test_persons_add_favourite
        mock_update, mock_context = test_update_context
        show_favourites(mock_update, mock_context)
        favourites_dto = PersonListDTO(owner.username, [favourite.username])
        assert mock_context.answer == favourites_dto.to_msg(BotMsg.FAVOURITE_BEGIN)
