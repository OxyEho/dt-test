import json
import uuid

import pytest
from django.utils import timezone

from app.internal.models.person import Person
from app.internal.models.token import Token
from app.internal.services.auth_service import REFRESH_EXPIRED
from app.internal.transport.bot.handlers import BotMsg, set_password
from app.internal.transport.rest import handlers


class TestAuthHandlers:
    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("qwerty!1AS#",))], indirect=True)
    def test_set_password(self, test_update_context):
        Person.objects.create(person_id=1, username="test_username")
        mock_update, mock_context = test_update_context
        set_password(mock_update, mock_context)
        assert mock_context.answer == BotMsg.SET_PASSWD

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_login_request", [("test_username", "qwerty!1AS#")], indirect=True)
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ("qwerty!1AS#",))], indirect=True)
    def test_login(self, test_login_request, test_update_context):
        Person.objects.create(person_id=1, username="test_username")
        mock_update, mock_context = test_update_context
        set_password(mock_update, mock_context)
        response = handlers.login(test_login_request)
        response_data = json.loads(response.content)
        assert response_data["username"] == "test_username"

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_login_request", [("test_username", "qwerty!1AS#")], indirect=True)
    def test_incorrect_password_login(self, test_login_request):
        Person.objects.create(person_id=1, username="test_username")
        response = handlers.login(test_login_request)
        response_data = json.loads(response.content)
        assert response_data["msg"] == "Incorrect password"

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_refresh_request", [("0d9aedad-8d96-44b3-9e5d-b077bf810fac",)], indirect=True)
    def test_refresh_token(self, test_refresh_request):
        refresh_time = timezone.now()
        person = Person.objects.create(person_id=1, username="test")
        token = Token.objects.create(
            access_token=uuid.UUID("48b14170-456c-45ef-8697-4572d9e090d5"),
            refresh_token=uuid.UUID("0d9aedad-8d96-44b3-9e5d-b077bf810fac"),
            person=person,
            creation_time=refresh_time,
        )
        response = handlers.refresh(test_refresh_request)
        response_data = json.loads(response.content)
        assert response_data["username"] == person.username
        assert token.access_token != uuid.UUID(response_data["access_token"])
        assert token.refresh_token != uuid.UUID(response_data["refresh_token"])

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_refresh_request", [("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",)], indirect=True)
    def test_incorrect_refresh_token(self, test_refresh_request):
        refresh_time = timezone.now()
        person = Person.objects.create(person_id=1, username="test")
        Token.objects.create(
            access_token=uuid.UUID("48b14170-456c-45ef-8697-4572d9e090d5"),
            refresh_token=uuid.UUID("0d9aedad-8d96-44b3-9e5d-b077bf810fac"),
            person=person,
            creation_time=refresh_time,
        )
        response = handlers.refresh(test_refresh_request)
        response_data = json.loads(response.content)
        assert response_data["msg"] == "Incorrect token"

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_refresh_request", [("0d9aedad-8d96-44b3-9e5d-b077bf810fac",)], indirect=True)
    def test_expired_refresh_token(self, test_refresh_request):
        refresh_time = timezone.now()
        person = Person.objects.create(person_id=1, username="test")
        Token.objects.create(
            access_token=uuid.UUID("48b14170-456c-45ef-8697-4572d9e090d5"),
            refresh_token=uuid.UUID("0d9aedad-8d96-44b3-9e5d-b077bf810fac"),
            person=person,
            creation_time=refresh_time - REFRESH_EXPIRED,
        )
        response = handlers.refresh(test_refresh_request)
        response_data = json.loads(response.content)
        assert response_data["msg"] == "Tokens expired"
