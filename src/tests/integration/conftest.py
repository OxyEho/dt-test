import json

import pytest

from app.internal.models.favorite import Favorite
from app.internal.models.person import Person


@pytest.fixture(scope="function")
def test_persons_not_add_favourite(
    first_person_id=1, first_username="test1", second_person_id=2, second_username="test2"
):
    first_person = Person.objects.create(person_id=first_person_id, username=first_username)
    second_person = Person.objects.create(person_id=second_person_id, username=second_username)
    return first_person, second_person


@pytest.fixture(scope="function")
def test_persons_add_favourite(first_person_id=1, first_username="test1", second_person_id=2, second_username="test2"):
    first_person = Person.objects.create(person_id=first_person_id, username=first_username)
    second_person = Person.objects.create(person_id=second_person_id, username=second_username)
    Favorite.objects.create(owner_id=first_person_id, favourite_id=second_person_id)
    return first_person, second_person


@pytest.fixture(scope="function")
def test_login_request(request, mocker):
    username, password = request.param
    request = mocker.MagicMock()
    request.configure_mock(**{"method": "POST", "body": json.dumps({"username": username, "password": password})})
    return request


@pytest.fixture(scope="function")
def test_refresh_request(request, mocker):
    refresh_token = request.param[0]
    request = mocker.MagicMock()
    request.configure_mock(**{"method": "POST", "body": json.dumps({"refresh_token": refresh_token})})
    return request
