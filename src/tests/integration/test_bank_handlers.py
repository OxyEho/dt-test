import pytest

from app.internal.transport.bot.handlers import BotMsg, account_balance, card_balance


class TestBankHandlers:
    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ())], indirect=True)
    def test_account_balance(self, test_person_with_account_card, test_update_context):
        person, bank_account, card = test_person_with_account_card
        mock_update, mock_context = test_update_context
        mock_context.args = [str(bank_account.account_id)]
        account_balance(mock_update, mock_context)
        assert mock_context.answer == BotMsg.get_account_cards_msg([card])

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ())], indirect=True)
    def test_existing_card_balance(self, test_person_with_account_card, test_update_context):
        person, bank_account, card = test_person_with_account_card
        mock_update, mock_context = test_update_context
        mock_context.args = [str(card.card_number)]
        card_balance(mock_update, mock_context)
        assert mock_context.answer == card.msg_str()

    @pytest.mark.django_db
    @pytest.mark.integration
    @pytest.mark.parametrize("test_update_context", [(1, "test_username", ())], indirect=True)
    def test_non_existing_card_balance(self, test_person_with_account_card, test_update_context):
        mock_update, mock_context = test_update_context
        mock_context.args = ["1" * 16]
        card_balance(mock_update, mock_context)
        assert mock_context.answer == BotMsg.CARD_NOT_FOUND
