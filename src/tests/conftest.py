import uuid
from decimal import Decimal

import pytest
from django.utils import timezone
from django.utils.timezone import now

from app.internal.models.bank_account import BankAccount
from app.internal.models.card import Card
from app.internal.models.person import Person
from app.internal.models.token import Token
from app.internal.services import auth_service


@pytest.fixture(scope="function")
def test_update_context(request, mocker):
    id_, username, context_args = request.param
    mock_update = mocker.MagicMock()
    mock_effective_user = mocker.MagicMock()
    mock_effective_user.configure_mock(**{"id": id_, "username": username})
    mock_update.configure_mock(**{"effective_user": mock_effective_user})
    mock_context = mocker.MagicMock()
    mock_bot = mocker.MagicMock()

    def mock_send_msg(*args, **kwargs):
        mock_context.answer = kwargs["text"]

    mock_bot.configure_mock(**{"send_message": mock_send_msg})
    mock_context.configure_mock(**{"args": [*context_args], "bot": mock_bot})
    return mock_update, mock_context


@pytest.fixture(scope="function")
def test_person(request):
    person_id, username, phone = request.param
    return Person.objects.create(person_id=person_id, username=username, phone=phone)


@pytest.fixture(scope="function")
def test_person_with_account_card(
    person_id=1, username="test", phone="77777777777", card_num="4216807515054906", balance=Decimal("1000.00")
):
    person = Person.objects.create(person_id=person_id, username=username, phone=phone)
    bank_account = BankAccount.objects.create(
        account_id=uuid.uuid4(),
        person=person,
        owner_name="test",
        owner_last_name="test",
        patronymic="test",
        open_date=now(),
    )
    card = Card.objects.create(
        card_number=card_num, account=bank_account, open_date=now(), currency="RUB", balance=balance
    )
    return person, bank_account, card


@pytest.fixture(scope="function")
def test_token():
    refresh_time = timezone.now()
    person = Person.objects.create(person_id=1, username="test")
    return Token.objects.create(
        access_token=uuid.uuid4(),
        refresh_token=uuid.uuid4(),
        creation_time=refresh_time,
        person=person,
    )


@pytest.fixture(scope="function")
def test_expired_token():
    refresh_time = timezone.now()
    person = Person.objects.create(person_id=1, username="test")
    return Token.objects.create(
        access_token=uuid.uuid4(),
        refresh_token=uuid.uuid4(),
        creation_time=refresh_time - auth_service.REFRESH_EXPIRED,
        person=person,
    )
