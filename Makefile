test:
	docker run --rm ${IMAGE_APP}
	make test_unit
	make test_integration
	make test_smoke

test_unit:
	docker-compose run --rm web bash -c "cd src && pytest -m unit"

test_integration:
	docker-compose run --rm web bash -c "cd src && pytest -m integration"

test_smoke:
	docker-compose run --rm web bash -c "cd src && pytest -m smoke"

lint:
	isort .
	flake8 --config setup.cfg
	black --target-version py310 --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --target-version py310 --config pyproject.toml .

ci_check_lint:
	docker run --rm ${IMAGE_APP}
	docker-compose run --rm web isort --check --diff .
	docker-compose run --rm web flake8 --config setup.cfg
	docker-compose run --rm web black --check --target-version py310 --config pyproject.toml .

build:
	docker-compose build

up:
	docker-compose up -d

collectstatic:
	docker-compose run web python src/manage.py collectstatic --no-input

down:
	docker-compose down

migrate_all:
	docker-compose run web python src/manage.py makemigrations app
	docker-compose run web python src/manage.py migrate

build_dev:
	docker-compose -f docker-compose-dev.yaml build

up_dev:
	docker-compose -f docker-compose-dev.yaml up -d

down_dev:
	docker-compose -f docker-compose-dev.yaml down

migrate_all_dev:
	docker-compose -f docker-compose-dev.yaml run web python src/manage.py makemigrations app
	docker-compose -f docker-compose-dev.yaml run web python src/manage.py migrate

createsuperuser_docker_dev:
	docker-compose -f docker-compose-dev.yaml run web python src/manage.py createsuperuser

make_all_dev:
	make build_dev
	make up_dev
	make migrate_all_dev

rm_db_volume:
	sudo docker volume rm dt-test_db_volume

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}